package com.nh.travelmeetingapi.entity;

import com.nh.travelmeetingapi.enums.coupon.IsUsed;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDate;
import java.util.UUID;

@Entity
@Getter
@Setter
public class Coupon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @UuidGenerator(style = UuidGenerator.Style.RANDOM)
    private UUID couponNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member; // 데이터를 보면서 직접 입력할지 보류

    @Column(nullable = false)
    private Boolean isUsed;

    @Column(nullable = false)
    private LocalDate dateUse;

    @Column(nullable = false)
    private LocalDate due;
}

package com.nh.travelmeetingapi.entity.matching;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class EventMatching {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchingId", nullable = false)
    private Matching matching;

    @Column(nullable = false)
    private Long applicant;

    @Column(nullable = false)
    private LocalDate dateAsk;
}

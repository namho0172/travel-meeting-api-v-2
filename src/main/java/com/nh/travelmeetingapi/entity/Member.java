package com.nh.travelmeetingapi.entity;

import com.nh.travelmeetingapi.enums.member.Grade;
import com.nh.travelmeetingapi.enums.member.State;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Boolean isNormal;

    @Column(nullable = false,length = 20, unique = true)
    private String account;

    @Column(nullable = false)
    private Long password;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private LocalDate dateJoin;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false,length = 35)
    private String eMail;

    @Column(nullable = false, length = 30)
    private String address;

    @Column(nullable = false, length = 15)
    @Enumerated(EnumType.STRING)
    private Grade grade;

    private String reason;// 정지랑 정지사유 하나로 묶기

    @Column(nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private State state;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

}

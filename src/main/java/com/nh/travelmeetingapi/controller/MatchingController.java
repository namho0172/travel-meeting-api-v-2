package com.nh.travelmeetingapi.controller;

import com.nh.travelmeetingapi.model.matching.MatchingCreateRequest;
import com.nh.travelmeetingapi.model.matching.MatchingItem;
import com.nh.travelmeetingapi.model.matching.MatchingResponse;
import com.nh.travelmeetingapi.model.matching.MatchingTypeChangeRequest;
import com.nh.travelmeetingapi.service.MatchingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/matching")
public class MatchingController {
    private final MatchingService matchingService;

    @PostMapping("/post")
    public String setMatching(@RequestBody MatchingCreateRequest request){
        matchingService.setMatching(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<MatchingItem> getMatchings(){
        return matchingService.getMatchings();
    }

    @GetMapping("/detail/{matchingId}")
    public MatchingResponse getMatching(@PathVariable long matchingId){
        return matchingService.getMatching(matchingId);
    }

    @PutMapping("/matching-type/{matchingId}")
    public String putMatchingType(@PathVariable long matchingId, MatchingTypeChangeRequest request){
        matchingService.putMatchingType(matchingId, request);

        return "ok";
    }

    @DeleteMapping("/{matchingId}")
    public String delMatching(@PathVariable long matchingId){
        matchingService.delMatching(matchingId);

        return "ok";
    }
}

package com.nh.travelmeetingapi.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum State {
    ACTIVITY("활동")
    ,PAUSE("일시정지")
    ,STOP("영구정지");
    private final String name;
}
